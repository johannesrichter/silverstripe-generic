<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 10/07/14
 * Time: 12:34 PM
 */

class ServerGetDumpFiles extends BuildTask {

	protected $title = 'assets&db snapshot';
	protected $description = 'Grab newest assets/ and database snapshot';
	protected $baseDirExtension = '../'; // not accessible from doc root
	protected $backupDir = 'snapshot';
	protected $downloadPath = '/devDump/download';

	/**
	 * Implement this method in the task subclass to
	 * execute via the TaskRunner
	 */
	public function run($request) {
		if(!Permission::check('ADMIN')) {
			$this->error('forbidden');
		}
		increase_time_limit_to();

		$dir = $this->getTargetDir();
		$assets = $dir . '/' . 'assets.zip';
		$db = $dir . '/' . 'db.zip';
		if ($this->createTargetDir()) {
			$this->assetsDump($assets);
			$this->mysqlDump($db);

			$this->ok('<a href="' . $this->downloadPath .'/assets">download assets</a>');
			$this->ok('<a href="' . $this->downloadPath .'/db">download database</a>');
		}
	}

	/**
	 * Run the actual assets dump
	 */
	protected function assetsDump($file) {
		$baseFolder = Director::baseFolder();

		$cmd = "cd '{$baseFolder}'; zip -r {$file} " . ASSETS_DIR . ";";
		exec($cmd);
	}

	/**
	 * Run the actual mysql dump
	 */
	protected function mysqlDump($file) {

		global $databaseConfig;

		$cmd = 'mysqldump -u'
		  . $databaseConfig['username']
		  . ' -p' . escapeshellarg($databaseConfig['password'])
		  . ' -h ' . $databaseConfig['server']
		  . ' ' . $databaseConfig['database']
		  . ' > ' . dirname($file) . '/db.sql && cd ' . dirname($file) . ' && zip ' . basename($file) . ' db.sql && rm db.sql';

		$this->runShell($cmd);
	}

	protected function runShell($cmd) {
		exec($cmd, $output, $status);
		return $output;
	}


	protected function error($message, $status = 1) {
		echo "<span style='color: red'>$message</span>";
		exit($status);
	}

	protected function ok($message) {
		echo "$message<br />";
	}

	protected function createTargetDir() {
		$baseFolder = Director::baseFolder();
		$targetDir = $this->getTargetDir();

		if (!is_dir($targetDir)) {
			$command = 'cd ' . $baseFolder . '/' . $this->baseDirExtension .' && mkdir ' . $this->backupDir;
			$this->runShell($command);
			if (!is_dir($targetDir)) {
				$this->error('Couldn\'t create directory.');
				return false;
			}
		}
		return true;
	}

	public function getTargetDir() {
		$baseFolder = Director::baseFolder();
		return $baseFolder . '/' . $this->baseDirExtension . $this->backupDir;
	}
}
