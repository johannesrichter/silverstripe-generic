<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 10/07/14
 * Time: 2:53 PM
 */

require_once(dirname(__FILE__) . '/../tasks/ServerGetDumpFiles.php');

class DevDumpDownloadController extends Controller {
	private static $allowed_actions = array(
		'download'
	);

	const CHUNK_SIZE = 2000; // in kB

	public function download(SS_HTTPRequest $request) {
		if(!Permission::check('ADMIN')) return 'forbidden';

		$params = $request->allParams();
		$name = isset($params['Name']) ? $params['Name'] : false;
		if (!$name) {
			die('no name');
		}

		$dir = ServerGetDumpFiles::create()->getTargetDir();
		$file = $dir . '/' . $name . '.zip';

		// this doesn't work with large files - idiots
		//return SS_HTTPRequest::send_file(file_get_contents($file), $name . '.tgz');
		$this->send_file($file, $name . '.zip');
	}

	protected function send_file($file_path, $name) {
		$fileSize = filesize($file_path);

		// Normal operation:
		$mimeType = HTTP::get_mime_type($name);
		header("Content-Type: {$mimeType}; name=\"" . addslashes($name) . "\"");
		header("Content-Disposition: attachment; filename=" . addslashes($name));
		header("Cache-Control: max-age=1, private");
		header("Content-Length: {$fileSize}");
		header("Pragma: ");

		if($filePointer = @fopen($file_path, 'rb')) {
			session_write_close();
			$this->flush();
			// Push the file while not EOF and connection exists
			while(!feof($filePointer) && !connection_aborted()) {
				print(fread($filePointer, 1024 * self::CHUNK_SIZE));
				$this->flush();
			}
			fclose($filePointer);
		}
		exit();
	}

	protected function flush() {
		if(ob_get_length()) {
			@ob_flush();
			@flush();
			@ob_end_flush();
		}
		@ob_start();
	}
}