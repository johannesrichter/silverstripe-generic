<?php

class RemarketingExtension extends DataExtension {

	private static $db = array(
		'FaceBookConvId' => 'Varchar(32)',
		'GoogleRemarketingCode' => 'Varchar(32)',
		'GoogleConvId' => 'Varchar(32)',
		'GoogleConvFormat' => 'Varchar(2)',
		'GoogleConvLabel' => 'Varchar(32)',
		'GoogleConvValue' => 'Varchar(32)'
	);

	public function updateCMSFields(FieldList $fields) {
		$ConversionFields = new FieldList(
			TextField::create('GoogleRemarketingCode', 'Google Remarketing Code')->setRightTitle('Code, e.g. 1072461839'),
			TextField::create('GoogleConvId', 'Google Conversion Tracking: ID')->setRightTitle('ID, e.g. 1072461839'),
			TextField::create('GoogleConvFormat', 'Google Conversion Tracking: Format')->setRightTitle('Format, e.g. 3'),
			TextField::create('GoogleConvLabel', 'Google Conversion Tracking: Label')->setRightTitle('Label, e.g. oQv_CN3rlVkQj_Cx_wM'),
			TextField::create('GoogleConvValue', 'Google Conversion Tracking: Value')->setRightTitle('Value, e.g. 1.00'),
			TextField::create('FaceBookConvId', 'FaceBook Conversion Tracking: ID')->setRightTitle('ID, e.g. 292461237611841')
		);
		if($this->owner->ClassName == 'SiteConfig') {
			$fields->addFieldsToTab(
				'Root.Remarketing',
				$ConversionFields
			);
		} else {
			$fields->addFieldToTab(
				'Root.Main',
				ToggleCompositeField::create(
					'Remarketing',
					'Remarketing',
					$ConversionFields
				)
			);
		}

	}
}
