<?php

define('MODULE_GENERIC_PATH', basename(dirname(__FILE__)));

// log errors and warnings
SS_Log::add_writer(new CustomLogEmailWriter('error-reporting@johannesrichter.com'), SS_Log::WARN, '<=');
