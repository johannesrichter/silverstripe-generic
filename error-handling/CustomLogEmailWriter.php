<?php

class CustomLogEmailWriter extends SS_LogEmailWriter {

	private static $send_from = 'error-reporting@johannesrichter.com';
	private static $notification_latency = 600;

	public function _write($event) {

		// exceptions
		if(is_array($event) && is_array($event['message']) && isset($event['message']['errstr'])) {
			if(strpos($event['message']['errstr'], 'DataObject::get_by_id passed a non-numeric ID #') === 0) {
				return;
			}
		}

		// If no formatter set up, use the default
		if(!$this->_formatter) {
			$formatter = new SS_LogErrorEmailFormatter();
			$this->setFormatter($formatter);
		}

		// this is our log file where we collect all the messages
		$EmailLogFile = getTempFolder().'/'.'error-mail.log';
		if(!file_exists($EmailLogFile)) {
			touch($EmailLogFile);
		}
		$EmailLockFile = getTempFolder().'/'.'error-last-mail-sent.log';
		if(!file_exists($EmailLockFile)) {
			touch($EmailLockFile);
			$FileMTime = 0;
		} else {
			$FileMTime = filemtime($EmailLockFile);
		}
		$Now = time();

		// write message to file
		$formattedData = $this->_formatter->format($event);
		$ToWrite = '<h1>'.DBField::create_field('SS_Datetime', $Now)->Format('Y-m-d H:i:s').' - '.$formattedData['subject'].'</h1>'.$formattedData['data'];
		$Handle = fopen($EmailLogFile, "r+");
		$Length = strlen($ToWrite);
		$FinalFileLength = filesize($EmailLogFile) + $Length;
		$Written = fread($Handle, $Length);
		rewind($Handle);
		$i = 1;
		while (ftell($Handle) < $FinalFileLength) {
			fwrite($Handle, $ToWrite);
			$ToWrite = $Written;
			$Written = fread($Handle, $Length);
			fseek($Handle, $i * $Length);
			$i++;
		}
		fclose($Handle);

		// send email if appropriate
		if (($FileMTime + self::$notification_latency) < $Now) {

			// format subject
			$siteconfig = SiteConfig::current_site_config();
			$subject = 'Error Reporting // '.$siteconfig->Title.' // ';
			if($FileMTime && $Now != $FileMTime) {
				$subject .= DBField::create_field('SS_Datetime', $FileMTime)->Format('Y-m-d H:i:s').' - ';
			}
			$subject .= DBField::create_field('SS_Datetime', $Now)->Format('Y-m-d H:i:s');

			// get data and reset file
			$data = file_get_contents($EmailLogFile);
			unlink($EmailLogFile);

			// update mail log
			touch($EmailLockFile);

			// set up email
			$from = Config::inst()->get('SS_LogEmailWriter', 'send_from');

			// override the SMTP server with a custom one if required
			$originalSMTP = ini_get('SMTP');
			if($this->customSmtpServer) ini_set('SMTP', $this->customSmtpServer);

			// Use plain mail() implementation to avoid complexity of Mailer implementation.
			// Only use built-in mailer when we're in test mode (to allow introspection)
			$mailer = Email::mailer();
			if($mailer instanceof TestMailer) {
				$mailer->sendHTML(
					$this->emailAddress,
					null,
					$subject,
					$data,
					null,
					"Content-type: text/html\nFrom: " . $from
				);
			} else {
				mail(
					$this->emailAddress,
					$subject,
					$data,
					"Content-type: text/html\nFrom: " . $from
				);
			}

			// reset the SMTP server to the original
			if($this->customSmtpServer) ini_set('SMTP', $originalSMTP);
		}

	}
}
