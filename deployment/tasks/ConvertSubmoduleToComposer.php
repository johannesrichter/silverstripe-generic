<?php

class ConvertSubmoduleToComposer extends BuildTask {

	protected $title = 'Convert Git Submodule To Composer';

	protected $gitignore = 'assets/*
dms-assets/*

silverstripe-*/
accessibility/
betterbuttons/
cms/
dependentdropdownfield/
disqus/
dms/
forum/
framework/
fulltextsearch/
googlesitemaps/
GridFieldBulkEditingTools/
gridfieldextensions/
livechat/
mssql/
multivaluefield/
queuedjobs/
redirectedurls/
SortableGridField/
spamprotection/
subsites/
taxonomy/
userforms/
vendor/

*combinedfiles*
BingSiteAuth.xml
google*.html
*sass-cache*
.idea
.DS_Store
';

	public function run($request) {
		increase_time_limit_to();
		if (chdir(BASE_PATH)) {
			if (Director::get_environment_type() == 'dev') {
				$this->convertRepo();
			}
		}

		echo '<br/> <br/> <br/>done';
	}

	public function convertRepo() {
		global $project;
		$ComposerFile = BASE_PATH.'/composer.json';
		if (file_exists($ComposerFile)) {
			die('no conversion needed');
		}

		$this->execute('cd '.BASE_PATH.' && git submodule foreach git reset --hard HEAD');

		$composer = array();
		$composer['require'] = array(
			"php" => ">=5.3.2",
			"johannesrichter/silverstripe-generic" => "dev-master"
		);
		$composer['repositories'] = array(
			array(
				"type" => "vcs",
				"url" => "git@bitbucket.org:johannesrichter/silverstripe-generic.git"
			)
		);
		$composer['config'] = array('process-timeout' => 600);
		$composer['minimum-stability'] = 'dev';
		$gitignore = explode("\n", $this->gitignore);

		foreach (gitmodules_get_all(BASE_PATH) as $submodule) {

			if (!in_array(
				$submodule->local_path,
				array(
					'silverstripe-generic',
					'silverstripe-meta',
					'silverstripe-dev_dump_download',
					'silverstripe-userforms_spam'
				))) {

				// composer file
				$version = $this->execute(
//					'cd '.BASE_PATH.'/'.$submodule->local_path.' && git rev-parse --abbrev-ref HEAD',
					'cd '.BASE_PATH.'/'.$submodule->local_path.' && git show-ref | grep $(git show-ref -s -- HEAD) | sed "s|.*/\(.*\)|\1|" | grep -v HEAD | sort | uniq',
					false
				);
				if ($version == 'master' || $version == 'HEAD') {
					$version = 'dev-master';
				} elseif ($version == '3.1') {
					$version = '3.1.x-dev';
				}
				elseif ($version == '3.0') {
					$version = '3.0.x-dev';
				}


				else {
					$version = '~'.$version;
				}
				if ($submodule->local_path == 'bootstrap-sass') {
					$version = 'dev-master#'.$this->execute(
							'cd '.BASE_PATH.'/'.$submodule->local_path.' && git rev-parse HEAD',
							false
						);
				}
				if (!$submodule->is_github) {
					$composer['repositories'][] = array(
						"type" => "vcs",
						"url" => $submodule->url
					);
				}

				// name
				$name = $this->getRepoName($submodule);

				// require
				$composer['require'][$name] = $version;

				// git
				if ($submodule->is_github && $submodule->local_path != 'bootstrap-sass') {
					$gitignore[] = $this->adjustLocalPath($submodule->local_path).'/';
				}
			}

			$this->execute('cd '.BASE_PATH.' && git submodule deinit -f '.$submodule->local_path);
			//$this->execute('cd '.BASE_PATH.' && git rm '.$submodule->local_path);
			$this->execute('cd '.BASE_PATH.' && rm -rf .git/modules/'.$submodule->local_path);
			$this->execute('cd '.BASE_PATH.' && rm -rf '.$submodule->local_path);
		}

		// composer file
		$contents = json_encode($composer, JSON_PRETTY_PRINT);
		$contents = str_replace('\/', '/', $contents);
		file_put_contents($ComposerFile, $contents);

		// git ignore
		$gitignore = array_unique($gitignore);
		file_put_contents(BASE_PATH.'/.gitignore', implode("\n", $gitignore));

		// sass paths
		$SassPath = BASE_PATH.'/themes/'.$project.'/sass/';
		foreach (
			array(
				$SassPath.'_includes.scss',
				$SassPath.'_variables.scss',
				$SassPath.'_variables_default.scss',
				$SassPath.'_variables_override.scss',
				$SassPath.'bootstrap.scss',
				$SassPath.'bootstrap1.scss'
			) as $SassFile) {
			if (file_exists($SassFile) && $Content = file_get_contents($SassFile)) {
				$Content = str_replace('../../../bootstrap-sass/', '../../../vendor/twbs/bootstrap-sass/', $Content);
				file_put_contents($SassFile, $Content);
			}
		}

		// finish
		$this->execute('cd '.BASE_PATH.' && rm -rf silverstripe-cache/*');
		$this->execute('cd '.BASE_PATH.' && composer update');
	}

	public function getRepoName($submodule) {
		// create author
		$author = isset($submodule->author) ? $submodule->author : 'johannesrichter';

		// path
		$path = $this->adjustLocalPath($submodule->local_path);

		// naming adjustments
		if ($author == 'silverstripe-labs') {
			$author = 'silverstripe';
		} elseif ($author == 'ajshort') {
			$author = 'silverstripe-australia';
		}

		return $author.'/'.$path;
	}

	public function adjustLocalPath($path) {
		if ($path == 'GridFieldBulkEditingTools') {
			$path = 'gridfield-bulk-editing-tools';
		} elseif ($path == 'SortableGridField') {
			$path = 'sortablegridfield';
		}

		return $path;
	}

	function execute($Command, $Print = true) {
		if ($Print) {
			echo "\n <br /><strong>".$Command."</strong>";
			@flush();
			@ob_flush();
		}
		$Return = '';
		$OutPut = array();
		exec($Command.' 2>&1', $OutPut);
		foreach ($OutPut as $Str) {
			if ($Print) {
				$Return .= "\n";
			}
			$Return .= $Str;
		}
		if ($Print) {
			$Return = nl2br($Return);
		}
		else {
			return $Return;
		}
	}
}

function gitmodules_get_all($dir = '') {
	$dir = rtrim($dir, '/\\');

	if (!file_exists($dir != '' ? $dir : '.')) {
		throw new Exception("Could not find directory $dir");
	}

	$gitmodules_path = ($dir != '' ? $dir : '.').'/.gitmodules';

	if (!file_exists($gitmodules_path)) {
		return array();
	}

	$contents = explode("\n", file_get_contents($gitmodules_path));

	$submodules = array();

	for ($i = 0; $i < count($contents); $i++) {
		$line = $contents[$i];

		if (($submodule_name = gitmodules_get_name($line))) {
			$submodule = new stdClass;

			$submodule->parent_path = $dir;

			$submodule->name = $submodule_name;
			$submodule->local_path = gitmodules_get_path($contents[++$i]);
			$submodule->url = gitmodules_get_url($contents[++$i]);

			$submodule->path = ($submodule->parent_path != '' ? $submodule->parent_path.'/' : '').$submodule->local_path;
			$submodule->path_exists = file_exists($submodule->path);

			$submodule->gitmodules_exists = $submodule->path_exists && file_exists($submodule->path.'/.gitmodules');

			$submodule->is_github = strpos($submodule->url, '://github.com') !== false;

			if ($submodule->is_github) {
				$submodule->author = gitmodules_get_author($submodule->url);
				$submodule->repo = gitmodules_get_repo($submodule->url);
			}

			$submodules[] = $submodule;
		}
	}

	return $submodules;
}

function gitmodules_get_by_name($name, $dir = '') {
	$submodules = gitmodules_get_all($dir);

	foreach ($submodules as $submodule) {
		if ($submodule->name == $name) {
			return $submodule;
		}
	}

	return null;
}

function gitmodules_get_name($line) {
	if (preg_match('@\[submodule "([^"]+)"\]@', $line, $matches)) {
		return $matches[1];
	} else {
		return false;
	}
}

function gitmodules_get_path($line) {
	if (preg_match('@\s+path\s+=\s+(.+)@', $line, $matches)) {
		return trim($matches[1]);
	} else {
		return false;
	}
}

function gitmodules_get_url($line) {
	if (preg_match('@\s+url\s+=\s+(.+)@', $line, $matches)) {
		return $matches[1];
	} else {
		return false;
	}
}

function gitmodules_get_author($submodule_url) {
	if (preg_match('@://github.com/([^/]+)/@', $submodule_url, $matches)) {
		return $matches[1];
	} else {
		return false;
	}
}

function gitmodules_get_repo($submodule_url) {
	if (preg_match('@://github.com/[^/]+/([^./]+)@', $submodule_url, $matches)) {
		return $matches[1];
	} else {
		return false;
	}
}

