<?php
class ServerGitPull extends BuildTask {

	protected $title = 'GIT pull';
	protected $description = 'Pull changes from GIT repository';

	public function run($request) {
		increase_time_limit_to();

		if (chdir(BASE_PATH)) {
			// if bitbucket not in known hosts file add
			$this->execute('cd '.BASE_PATH.' && ssh-keygen -H -F bitbucket.org || ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts');
			$this->execute('cd '.BASE_PATH.' && git fetch && git status && git pull');

			echo '<br /><br />';
			$this->execute('cd '.BASE_PATH.' && git submodule update --init');

			if (file_exists(BASE_PATH.'/composer.json')) {
				echo '<br /><br />';
				$user = posix_getpwuid(posix_getuid());
				putenv("COMPOSER_HOME=".$user['dir']."/.composer");
				$this->execute('cd '.BASE_PATH.' && composer install');
			}

			echo '<br /><br />';
			$this->execute('cd '.BASE_PATH.' && rm -rf silverstripe-cache/*');

			global $_FILE_TO_URL_MAPPING;
			if (!isset($_FILE_TO_URL_MAPPING[BASE_PATH])) {
				echo '<br /><br /><p style="color: red;">You should create a $_FILE_TO_URL_MAPPING for '.BASE_PATH.'</p>';
			} else {
				echo '<br /><br />';
				$this->execute('cd '.BASE_PATH.' && ./framework/sake dev/build');
			}
		} else {
			echo "could not change directory to document root";
		}

		echo '<br /><br /><a href="/dev/tasks">back to tasks</a>';
	}

	function execute($Command, $Print = true) {
		echo "\n > <strong>".$Command."</strong>";
		@flush();
		@ob_flush();

		$Return = '';
		$OutPut = array();
		exec($Command.' 2>&1', $OutPut);
		$Exit = false;
		foreach ($OutPut as $Str) {
			$Return .= "\n".$Str;
			if (strpos($Str, 'error') !== false || strpos($Str, 'fatal') !== false || strpos(
					$Str,
					'is not recognized as an internal or external command'
				)
			) {
				$Exit = true;
			}
		}
		$Return = nl2br($Return);
		if ($Print || $Exit) {
			echo $Return;
			@flush();
			@ob_flush();
		}
		if ($Exit) {
			die();
		}
	}

}
